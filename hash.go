// Copyright 2014-2019 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//
// hash.go implements a chess transposition table.
//
// Position is hashed using a Zobrist-like algorithm.
// The lower (up to) 32 bits are used for indexing.
// The higher 32 bits (lock) are used for collision detection.
// Each node can be mapped into two slots. When inserting
// a new node we evict an older node based on some criteria.

package main

import (
	"unsafe"

	. "bitbucket.org/zurichess/board"
)

const (
	DefaultHashTableSizeMB = 32
)

var (
	hashLogSize uint32
	hashSize    uint32
	hashMask    uint32
	hashTable   []Node
)

// hashLock returns 32 bits of hash to detect collisions (lock).
func hashLock(pos *Position) uint32 {
	return uint32(pos.Zobrist() >> 32)
}

// hashIndex returns the two slotes where the position/node can be inserted.
func hashIndex(pos *Position) (uint32, uint32) {
	z := uint32(pos.Zobrist())
	i1 := z & hashMask
	i2 := i1 ^ (z >> 28)
	return i1, i2
}

// hashGetPtr returns a pointer to the node associated with the
// position. Ok if the node returned at ptr is correct, and
// false if it's another node. Caller can overwrite the node.
func hashGetPtr(pos *Position) (*Node, bool) {
	i1, i2 := hashIndex(pos)
	lock := hashLock(pos)
	if hashTable[i1].lock == lock {
		return &hashTable[i1], true
	}
	if hashTable[i2].lock == lock {
		return &hashTable[i2], true
	}
	if hashTable[i1].result.points > hashTable[i2].result.points {
		i1, i2 = i2, i1
	}
	return &hashTable[i1], false
}

// hashGet is similar to hashGetPtr, except it returns a value
// and doesn't override the hash entry.
func hashGet(pos *Position) Node {
	if ptr, ok := hashGetPtr(pos); ok {
		return *ptr
	} else {
		return MakeNode(pos)
	}
}

// hashPut puts a new node in the table.
func hashPut(pos *Position, node Node) {
	ptr, _ := hashGetPtr(pos)
	*ptr = node
}

// hashClear clears the hash table.
func hashClear() {
	for i := range hashTable {
		hashTable[i] = Node{}
	}
}

// hashSmooth halves all results in the table.
func hashSmooth() {
	for i := range hashTable {
		r := hashTable[i].result
		if r.total >= 8 {
			hashTable[i].result.points = (r.points + r.total%2) / 2
			hashTable[i].result.total = (r.total + 1) / 2
		}
	}
}

// hashSetSize sets the size of the table to fit mib megibytes.
func hashSetSize(mib uint64) {
	hashLogSize = 4
	// Assume that each node is child of at least another node
	// and reserve memory accordingly.
	nodeSize := uint64(unsafe.Sizeof(Node{})) + uint64(unsafe.Sizeof(Child{}))
	for (2<<hashLogSize)*nodeSize <= mib<<20 {
		hashLogSize++
	}

	hashSize = 1 << hashLogSize
	hashMask = hashSize - 1
	hashTable = make([]Node, hashSize)
}

func init() {
	hashSetSize(DefaultHashTableSizeMB)
}
