#!/usr/bin/python
# Copyright 2014-2018 The Zurichess Authors. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

"""Tensorflow program to train zurichess evaluation function.

Usage:
    % python tf.py --steps 1000 --input testdata

Input is CSV file each line contains:
    result, feature1, ..., featureN

"""

import sys
import argparse
import numpy as np
import tensorflow as tf


FLAGS = None


def build_nn(midgame, endgame, data, name, is_training):
    """Builds a neural network.

    Returns:
        error - error of evaluation
        loss - error + regularization for minimizing

    """

    y_data = data[:, 0]
    p_data = data[:, 1]
    x_data = data[:, 2:]

    # Build the network.
    phase = tf.constant(p_data, name='phase')
    ym = tf.reduce_sum(x_data * midgame, 1) * (1 - phase)
    ye = tf.reduce_sum(x_data * endgame, 1) * (phase)
    y = (ym + ye) / 2

    error = tf.reduce_mean(tf.square(tf.sigmoid(y) - y_data))
    error += tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=y_data, logits=y))

    if is_training:
        regularization = 1e-4 * tf.reduce_mean(midgame*midgame + endgame*endgame)
        #regularization = 1e-3 * tf.reduce_mean(tf.abs(midgame) + tf.abs(endgame))
        loss = error + regularization
    else:
        loss = error

    return error, loss


def train():
    """Sets up the network."""

    session = tf.InteractiveSession()

    data = np.genfromtxt(FLAGS.input, dtype='float32', filling_values=0, invalid_raise=False)
    num_features = len(data[0]) - 2
    midgame = tf.Variable(tf.random_uniform([num_features]), name='midgame')
    endgame = tf.Variable(tf.random_uniform([num_features]), name='endgame')

    split = len(data) * 4 / 5
    train_data = data[:split]
    train_error, train_loss = build_nn(midgame, endgame, train_data, 'train', True)
    valid_data = data[split:]
    valid_error, valid_loss = build_nn(midgame, endgame, valid_data, 'valid', False)

    optimizer = tf.train.AdamOptimizer(learning_rate=0.05)
    train = optimizer.minimize(train_loss)
    session.run(tf.global_variables_initializer())

    for step in xrange(0, FLAGS.steps):
        session.run(train)
        if step == FLAGS.steps-1 or step % 200 == 0:
            train_, valid_ = session.run([train_error, valid_error])
            print step, '; train error =', train_, '; validation error =', valid_

            mid, end = session.run([midgame, endgame])

            print "\tmidgame = [2*numFeatures]int32{",
            for i in xrange(len(mid)):
                if i%12 == 0:
                    print
                    print "\t\t",
                print "%+6d, " % int(mid[i]*10000),
            print
            print "\t}"

            print "\tendgame = [2*numFeatures]int32{",
            for i in xrange(len(end)):
                if i%12 == 0:
                    print
                    print "\t\t",
                print "%+6d, " % int(end[i]*10000),
            print
            print "\t}"

    session.close()


def main(_):
    train()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str, required=True,
                        help='Input csv file')
    parser.add_argument('--steps', type=int, default=2000,
                        help='Number of training steps to run')
    parser.add_argument('--output', type=str, default='',
                        help='File to print write weights to')
    FLAGS = parser.parse_args()
    tf.app.run()
