// Copyright 2014-2019 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"math"

	. "bitbucket.org/zurichess/board"
)

type Options struct {
}

type Stats struct {
	Depth    int
	SelDepth int
	Nodes    uint64
}

var (
	Win  = Result{points: 8, total: 8}
	Draw = Result{points: 4, total: 8}
	Loss = Result{points: 0, total: 8}
)

// Result stores a match statistics
type Result struct {
	points uint32
	total  uint32
}

// winChance returns the winning probability.
func (r Result) winChance() float64 {
	// Use Laplace smoothing, shift the probability towards draws a bit.
	// Smoothing coefficient is scaled by the number of points per game.
	return float64(r.points+5) / float64(r.total+10)
}

// numGames returns the number of games.
func (r Result) numGames() uint32 {
	return r.total / 8
}

// reverse returns the result from other player's POV.
func (r Result) reverse() Result {
	return Result{points: r.total - r.points, total: r.total}
}

// merge merges other results.
func (r *Result) merge(o Result) {
	r.points += o.points
	r.total += o.total
}

// mergeN merges other results N times.
func (r *Result) mergeN(o Result, n uint32) {
	r.points += n * o.points
	r.total += n * o.total
}

// Logger logs search progress.
type Logger interface {
	// BeginSearch signals a new search is started.
	BeginSearch()
	// EndSearch signals end of search.
	EndSearch()
	// PrintPV logs the principal variation after iterative deepening completed one depth.
	PrintPV(stats Stats, multiPV int, score int32, pv []Move)
	// CurrMove logs the current move. Current move index is 1-based.
	CurrMove(depth int, move Move, num int)
}

// NulLogger is a logger that does nothing.
type NulLogger struct{}

func (nl *NulLogger) BeginSearch()                                             {}
func (nl *NulLogger) EndSearch()                                               {}
func (nl *NulLogger) PrintPV(stats Stats, multiPV int, score int32, pv []Move) {}
func (nl *NulLogger) CurrMove(depth int, move Move, num int)                   {}

type Engine struct {
	Options  Options   // engine options
	Log      Logger    // logger
	Stats    Stats     // search statistics
	Position *Position // current Position
	buffer   []Move    // a buffer to avoid expensive allocations
}

func NewEngine(pos *Position, log Logger, options Options) *Engine {
	e := &Engine{
		Options: options,
		Log:     log,
		buffer:  make([]Move, 0, 1000),
	}
	e.SetPosition(pos)
	return e
}

// SetPosition sets current position.
// Should be called before PlayMoves.
func (e *Engine) SetPosition(pos *Position) {
	if pos != nil {
		e.Position = pos
	} else {
		e.Position, _ = PositionFromFEN(FENStartPos)
	}
}

func (e *Engine) DoMove(m Move) {
	e.Position.DoMove(m)
}

func (e *Engine) UndoMove() {
	e.Position.UndoMove()
}

// debug prints information for position.
func (e *Engine) debug(pos *Position, watch string) {
	root := hashGet(pos)
	ptr := &root

	fmt.Printf("info string result: %+v\n", ptr.result)
	fmt.Print("info string children: ")
	l := 0.065 * math.Sqrt(float64(ptr.result.numGames()))
	for _, n := range ptr.children {
		if n.move.String() == watch {
			fmt.Printf("\033[1m")
		}
		node := n.getOrCreateNode(pos)
		puct := (1 - node.result.winChance()) + (1-node.weight)*l/float64(1+node.result.numGames())
		if node.checkMate == 0 {
			fmt.Printf("%v (%.5f, %.5f); ", n.move, node.result.winChance(), puct)
		} else {
			fmt.Printf("%v (%.5f, %.5f, %d); ", n.move, node.result.winChance(), puct, node.checkMate)
		}
		if n.move.String() == watch {
			fmt.Printf("= %d/%d; ", node.result.points, node.result.total)
			fmt.Printf("\033[0m")
		}
	}
	fmt.Println()
}

func (e *Engine) search(depth int) Result {
	e.Stats.Nodes++
	pos := e.Position

	// Update seldepth.
	if depth > e.Stats.SelDepth {
		e.Stats.SelDepth = depth
	}

	node := hashGet(pos)

	// Test endgames.
	if pos.FiftyMoveRule() || pos.InsufficientMaterial() {
		node.result.merge(Draw)
		hashPut(pos, node)
		return Draw
	}
	if r := pos.ThreeFoldRepetition(); r >= 3 || depth > 0 && r >= 2 {
		node.result.merge(Draw)
		hashPut(pos, node)
		return Draw
	}

	// If node is not explored yet MCTS says we should play many
	// rollouts. In practice, for chess at least, using the evaluation is
	// faster and more precise.
	if node.exploit < 2 {
		node.exploit++
		result := Result{uint32(node.weight*9 + 0.5), 9}
		node.result.merge(result)
		hashPut(pos, node)
		return result
	}

	// If we are deep enough do multiple searches.
	num := 1
	if depth <= 3 || (node.weight > 0.75 && pos.IsChecked(pos.Us())) {
		num = 3
	}

	var result Result
	for ; num > 0; num-- {
		// Pick a child and make the move.
		child, move := node.pickChild(pos, depth, e.buffer)
		if child == nil {
			if pos.IsChecked(pos.Us()) { // Cannot escape the check.
				result.mergeN(Loss, uint32(num))
				node.checkMate = -1
				node.weight = 0.0
			} else { // Stale mate.
				result.mergeN(Draw, uint32(num))
			}
			break
		}

		pos.DoMove(move)
		r := e.search(depth + 1).reverse()
		pos.UndoMove()

		// If the child is a lost position, then this is a win.
		if child.checkMate < 0 {
			result.merge(Win)
			node.weight = 1.0
			if node.checkMate == 0 || -child.checkMate+1 < node.checkMate {
				node.checkMate = -child.checkMate + 1
			}
		}

		// If all children are winning, then this position is lost.
		if child.checkMate > 0 {
			result.merge(Loss)
			longest := child.checkMate
			for i := range node.children {
				if c := node.children[i].checkMate(); c > longest {
					longest = c
				} else if c <= 0 {
					longest = 0
					break
				}
			}

			if longest > 0 {
				node.weight = 0.0
				node.checkMate = -longest - 1
			}
		}

		if child.checkMate == 0 {
			result.merge(r)
		}
	}

	node.result.merge(result)
	hashPut(pos, node)
	return result
}

func (e *Engine) PlayMoves(tc *TimeControl, rootMoves []Move) (score int32, moves []Move) {
	if len(rootMoves) != 0 {
		panic("searchmoves not supported")
	}

	e.Stats = Stats{}
	e.Log.BeginSearch()
	pos := e.Position

	for t := 0; !tc.Stopped() || hashGet(pos).exploit < 2; t++ {
		e.Stats.Depth = t
		e.search(0)

		if t&(t-1) == 0 {
			if hashGet(pos).result.total > 1e9 {
				hashSmooth()
			}

			score, moves = e.getPrincipalVariation()
			e.Log.PrintPV(e.Stats, 1, score, moves)
			if len(hashGet(pos).children) == 1 {
				break // Stop search when there is only one possible root move.
			}
		}
	}

	// Follow the best moves from root.
	score, moves = e.getPrincipalVariation()
	e.Log.PrintPV(e.Stats, 1, score, moves)

	e.Log.EndSearch()
	return score, moves
}

// getPrincipalVariation computes the principal variation.
// Returns current score and best moves.
func (e *Engine) getPrincipalVariation() (int32, []Move) {
	var moves []Move
	visited := make(map[uint64]bool)
	pos := e.Position
	node := hashGet(pos)
	score := probabilityToCentiPawns(node.result.winChance())

	for !visited[pos.Zobrist()] {
		visited[pos.Zobrist()] = true
		node.genChildren(pos, e.buffer)
		node.sortChildren()
		if len(node.children) == 0 {
			break
		}

		child := &node.children[0]
		if ptr := child.tryGetNode(); ptr == nil && len(moves) > 0 {
			break
		}

		node = *child.getOrCreateNode(pos)
		moves = append(moves, child.move)
		pos.DoMove(child.move)
	}

	for range moves {
		pos.UndoMove()
	}

	return score, moves
}

func probabilityToCentiPawns(p float64) int32 {
	// To convert from probability to centipawn we can use
	// either inverse sigmoid or math.Tan. The later is used
	// by Leela. math.Tan(3.0*(p-0.5)) has a range of values between
	// -14.10 and +14.10. The score is scaled to return
	// approx 260 for this position https://lichess.org/gEScf0YE/black#83.
	return int32(130 * math.Tan(3.0*(p-0.5)))
}
