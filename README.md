# zurichess: a chess engine

[www.zurichess.xyz](http://www.zurichess.xyz)

zurichess is a chess engine and a chess library written in
[Go](http://www.golang.org). Its main goals are: to be a relatively
strong chess engine and to enable chess tools writing. See
the library reference.

zurichess is NOT a complete chess program. Like with most
other chess engines you need a GUI that supports the UCI
protocol. Some popular GUIs are XBoard (Linux), Eboard (Linux)
Winboard (Windows), Arena (Windows).

zurichess partially implements [UCI
protocol](http://wbec-ridderkerk.nl/html/UCIProtocol.html), but
the available commands are enough for most purposes. zurichess was
successfully tested under Linux AMD64 and Linux ARM7 and other people
have tested zurichess under Windows AMD64.

You can play against zurichess on [LiChess](https://lichess.org), by
challenging [BOT less_than_one](https://lichess.org/@/less_than_one).
Usually it runs code at tip (master) which is a bit stronger
than the latest stable version.

## Disclaimer

This project is not associated with my employer.
