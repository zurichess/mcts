package main

import (
	"math"

	. "bitbucket.org/zurichess/board"
)

const (
	numFeatures = 112
)

var (
	midgame = [2 * numFeatures]int32{
		+0, +3713, +10075, +13174, +32299, +45202, +0, +3142, +1195, +12695, +14884, +14327,
		+16348, +856, +12911, +14076, +13520, +15171, +709, +941, +0, +4349, +4003, +4918,
		+4174, +10326, +16814, +0, +929, -1646, -684, +686, +794, +1290, +1483, -623,
		+4779, -1518, -4270, -6796, +3524, +9128, +2980, -9371, +2208, +8839, +5989, -647,
		+2427, -1684, +8194, +6995, -588, +1333, -1832, -458, -751, -1232, +3830, +2594,
		+1796, -1688, +7120, +1112, +11080, +7631, +8510, +1983, +0, +149, -14, +6732,
		+11924, +12143, +0, +701, +5041, +5492, +9707, +18302, +0, -307, +2423, +4117,
		+3469, +21858, +0, -250, +282, +2785, +137, +1175, +0, -420, -8322, +1731,
		-15566, +7061, +0, +542, +1269, +1314, -3185, -760, -1398, +1103, -12528, -25097,
		-2218, -896, -1314, +0, +0, +3990, +11419, +17234, +31477, +44789, +0, +3266,
		+967, +11880, +14159, +14325, +16662, +936, +8568, +9881, +9342, +9964, +807, +940,
		+0, +1235, +1185, +1903, +1585, +5807, +16911, +0, +3479, +1611, +2652, +3759,
		+3754, +5177, +4806, +2509, +3604, -3135, -6117, -7208, -3717, +4748, -12730, +9763,
		+4651, +6792, +4364, -3532, +606, -1407, +6888, +5194, +369, +1372, -1972, -261,
		-5472, -8468, +4449, +2817, +1493, -1871, +9653, +526, +5057, +3751, +629, +3427,
		+17994, -222, -1942, +5070, +7513, +5081, +13612, +9, +2911, +4888, +1651, +8700,
		+13240, -169, +1451, +4165, +963, +8328, +14878, -470, -370, +1061, +84, +117,
		+14305, -1323, -5559, -2041, -16375, +0, +0, +858, +3159, +3423, +1614, +1773,
		+0, -7470, -7252, -16499, -4134, -14136, -17557, +31131,
	}
	endgame = [2 * numFeatures]int32{
		+0, +1976, +10959, +8670, +38408, +95464, +0, +259, +822, +3542, +5802, +7931,
		+10050, +724, +8467, +11276, +13286, +11847, +679, +173, +0, +2062, +1332, +1357,
		+4695, +8969, +19785, +0, +7040, +7282, +5616, +4629, +4233, +4532, +4139, +4473,
		-1805, +3872, +4822, +5494, +5522, +5951, +10157, +6799, +516, +1899, +4452, +6532,
		+6545, +7594, +4128, +2079, +4807, -494, -2891, -526, -3983, -9792, -550, -2434,
		+742, +2255, -642, -3997, +3009, +8047, -15728, +51821, +0, +2371, +1101, +7884,
		+9239, +64123, +0, +2770, +5096, +2027, +13759, +61924, +0, +4488, +3619, +5977,
		-16360, +54661, +0, +5526, +6683, +3958, +8478, -1114, +0, +4902, +1868, +3361,
		+1823, +48776, +0, +2930, +3372, +5407, +6467, +3174, -445, -4196, -2500, -818,
		-1352, +550, +2578, +0, +0, +3725, +9653, +12101, +36725, +91645, +0, -1081,
		+578, +5560, +8443, +11306, +12587, +373, +7720, +9834, +12963, +12934, +813, +185,
		+0, +1401, +1148, +1590, +4686, +9457, +19875, +0, +5211, +4189, +3062, +2253,
		+2249, +2164, +1978, +2826, -2666, +3883, +5048, +5350, +7214, +6461, +10572, +1845,
		+2210, +3592, +5425, +7945, +6751, +7242, +4433, +2190, +3515, -131, -3011, -570,
		-8368, -34790, -1326, -2225, +923, +3260, -807, -3889, +1670, +6195, +7413, +2462,
		+4211, +3141, +478, +5811, +6809, +19550, +4308, +3458, +5444, +394, +8495, +491,
		+5446, +5182, +6089, +5207, -13819, +18085, +3557, +7759, +8521, +6770, +10522, +304,
		+9830, +5917, +1454, +3864, +5076, +0, +0, +2058, +1108, +5823, +4301, +3317,
		+0, -2799, -9647, -8745, -2772, -3048, -25709, +44436,
	}
)

var (
	// edgeDistance stores for each square the distance to the closest edge.
	edgeDistance = [SquareArraySize]int32{
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 1, 1, 1, 1, 1, 1, 0,
		0, 1, 2, 2, 2, 2, 1, 0,
		0, 1, 2, 3, 3, 2, 1, 0,
		0, 1, 2, 3, 3, 2, 1, 0,
		0, 1, 2, 2, 2, 2, 1, 0,
		0, 1, 1, 1, 1, 1, 1, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
	}
)

type evaluator struct {
	features [2 * numFeatures]int32
	attack   [ColorArraySize][FigureArraySize]Bitboard
	position *Position
}

// getMaterialFeatures updates features with values for us.
func (e *evaluator) getMaterialFeatures(us Color, features []int32) {
	pos := e.position
	them := us.Opposite()

	// #0. Piece counts.
	features[0] += 0
	features[1] += Pawns(pos, us).Count()
	features[2] += Knights(pos, us).Count()
	features[3] += Bishops(pos, us).Count()
	features[4] += Rooks(pos, us).Count()
	features[5] += Queens(pos, us).Count()
	features[6] += 0

	// #7. Bishop pair.
	features[7] += Bishops(pos, us).Count() / 2

	// #8. Mobility.
	pawns := Pawns(pos, us)
	danger := PawnThreats(pos, them)
	all := pos.ByColor(White) | pos.ByColor(Black)

	e.attack[us][Pawn] = PawnThreats(pos, us)
	e.attack[us][King] = KingMobility(Kings(pos, us).AsSquare())

	for bb := Knights(pos, us); bb > 0; {
		sq := bb.Pop()
		mob := KnightMobility(sq)
		e.attack[us][Knight] |= mob
		features[8] += (mob &^ danger &^ pawns).Count()
		features[9+edgeDistance[sq]]++
	}

	for bb := Bishops(pos, us); bb > 0; {
		sq := bb.Pop()
		mob := BishopMobility(sq, all)
		e.attack[us][Bishop] |= mob
		features[13] += (mob &^ danger &^ pawns).Count()
		features[14+edgeDistance[sq]]++
	}

	for bb := Rooks(pos, us); bb > 0; {
		sq := bb.Pop()
		mob := RookMobility(sq, all)
		e.attack[us][Rook] |= mob
		features[18] += (mob &^ danger &^ pawns).Count()
	}

	for bb := Queens(pos, us); bb > 0; {
		sq := bb.Pop()
		mob := QueenMobility(sq, all)
		e.attack[us][Queen] |= mob
		features[19] += (mob &^ danger &^ pawns).Count()
	}

	// #15. Advanced pawns.
	for bb := pos.ByPiece(us, Pawn); bb != 0; {
		sq := bb.Pop().POV(us)
		features[20+sq.Rank()]++
		features[28+sq.File()]++
	}

	// #31. King's position
	{
		kSq := Kings(pos, us).AsSquare().POV(us)
		features[36+kSq.Rank()]++
		features[44+kSq.File()]++
	}

	// #47. Pawn structure.
	features[52] = PassedPawns(pos, us).Count()
	features[53] = ConnectedPawns(pos, us).Count()
	features[54] = DoubledPawns(pos, us).Count()
	features[55] = IsolatedPawns(pos, us).Count()

	// #51. Pawn threats.
	features[56] = (danger & Minors(pos, us)).Count()
	features[57] = (danger & Majors(pos, us)).Count()

	// #53. Rook on open and semi-open files.
	features[58] = (Rooks(pos, us) & OpenFiles(pos, us)).Count()
	features[59] = (Rooks(pos, us) & SemiOpenFiles(pos, us)).Count()

	// #55. King safety.
	// TODO: move attacks into getAttackFeatures
	features[60] = (kingShield(pos, us) & Pawns(pos, us)).Count()
	features[61] = (KingArea(pos, us) & PassedPawns(pos, us)).Count()
	features[62] = (KingArea(pos, us) & PassedPawns(pos, them)).Count()

	for f := FigureMinValue; f <= FigureMaxValue; f++ {
		e.attack[us][NoFigure] |= e.attack[us][f]
	}
}

func (e *evaluator) getAttackFeatures(us Color, features []int32) {
	pos := e.position
	them := us.Opposite()

	// #63. Figures attacking figures.
	s := 63
	for f1 := FigureMinValue; f1 <= FigureMaxValue; f1++ {
		for f2 := FigureMinValue; f2 <= FigureMaxValue; f2++ {
			features[s] += (e.attack[us][f1] & pos.ByPiece(them, f2)).Count()
			s++
		}
	}

	// #99. Attacked pieces, but protected by pawns.
	s = 99
	for f1 := FigureMinValue; f1 <= FigureMaxValue; f1++ {
		features[s] += (pos.ByPiece(us, f1) & e.attack[them][NoFigure] & e.attack[us][Pawn]).Count()
		s++
	}

	// #105. Unprotected pieces in enemy king's attack radius.
	if Pawns(pos, us)&^e.attack[us][NoFigure]&e.attack[them][King] != 0 {
		features[105]++
	}
	if Minors(pos, us)&^e.attack[us][NoFigure]&e.attack[them][King] != 0 {
		features[106]++
	}
	if Majors(pos, us)&^e.attack[us][NoFigure]&e.attack[them][King] != 0 {
		features[107]++
	}
	// #103. Hanging minors attacked by the enemy.
	if Pawns(pos, us)&^e.attack[us][NoFigure]&e.attack[them][NoFigure] != 0 {
		features[108]++
	}
	if Minors(pos, us)&^e.attack[us][NoFigure]&e.attack[them][NoFigure] != 0 {
		features[109]++
	}
	if Majors(pos, us)&^e.attack[us][NoFigure]&e.attack[them][NoFigure] != 0 {
		features[110]++
	}
	// #104. Queen contact check.
	if Queens(pos, us)&e.attack[us][NoFigure]&e.attack[them][King] != 0 {
		features[111]++
	}
}

// getFeatures extractes features from current players POV.
func (e *evaluator) getFeatures() {
	e.getMaterialFeatures(e.position.Us(), e.features[:numFeatures])
	e.getMaterialFeatures(e.position.Them(), e.features[numFeatures:])

	e.getAttackFeatures(e.position.Us(), e.features[:numFeatures])
	e.getAttackFeatures(e.position.Them(), e.features[numFeatures:])

	for i := numFeatures; i < 2*numFeatures; i++ {
		e.features[i] = -e.features[i]
	}
}

// evaluate returns the position score before the final
// activation function and multiplied by 1e4.
func evaluate(pos *Position) int32 {
	eval := evaluator{position: pos}
	eval.getFeatures()

	var mid, end int32
	for i, v := range eval.features {
		mid += v * midgame[i]
		end += v * endgame[i]
	}
	p := phase(pos)
	return (mid*(256-p) + end*p) / 256
}

// sigmoid computes the mathematical sigmoid function of a evaluation score.
func sigmoid(e int32) float64 {
	x := float64(e) * 1e-4
	return 1 / (1 + math.Exp(-x))
}

// Phase computes the progress of the game.
// 0 is opening, 256 is late end game.
func phase(pos *Position) int32 {
	total := int32(4*1 + 4*1 + 4*3 + 2*6)
	curr := total
	curr -= pos.ByFigure(Knight).Count() * 1
	curr -= pos.ByFigure(Bishop).Count() * 1
	curr -= pos.ByFigure(Rook).Count() * 3
	curr -= pos.ByFigure(Queen).Count() * 6
	if curr < 0 {
		curr = 0
	}
	return (curr*256 + total/2) / total
}

// kingShield computes king's shield which is made
// of the 3x3 squares in front of the king.
func kingShield(pos *Position, us Color) Bitboard {
	return Forward(us, KingArea(pos, us))
}
