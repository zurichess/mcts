// Copyright 2014-2018 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/pprof"
	"strconv"
	"strings"

	. "bitbucket.org/zurichess/board"
)

var (
	cpuprofile    = flag.String("cpuprofile", "", "write cpu profile to file")
	convertSample = flag.String("convert_sample", "", "input sample file to convert to features")

	buildVersion = "mcts"
)

func main() {
	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if *convertSample != "" {
		convertSampleFile(*convertSample)
		return
	}

	fmt.Printf("zurichess %v http://www.zurichess.xyz\n", buildVersion)
	fmt.Printf("build with %v, running on %v\n", runtime.Version(), runtime.GOARCH)

	log.SetOutput(os.Stdout)
	log.SetPrefix("info string ")
	log.SetFlags(log.Lshortfile)

	uci := NewUCI()
	scan := bufio.NewScanner(os.Stdin)
	for scan.Scan() {
		line := scan.Text()
		if err := uci.Execute(line); err != nil {
			if err != errQuit {
				log.Println(err)
			} else {
				break
			}
		}
	}

	if scan.Err() != nil {
		log.Println(scan.Err())
	}
}

// convertSampleFile converts the file with sample positions
// to features useful for training.
func convertSampleFile(path string) {
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	// Read input file line by line.
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Fields(line)
		if len(fields) < 6 {
			log.Println("ignoring line:", "not enough FEN fields")
			continue
		}

		fen := strings.Join(fields[:6], " ")

		result, err := strconv.ParseFloat(fields[6], 64)
		if err != nil {
			log.Println("ignoring line:", err)
			continue
		}

		pos, err := PositionFromFEN(fen)
		if err != nil {
			log.Println("ignoring line:", err)
			continue
		}

		eval := evaluator{position: pos}
		eval.getFeatures()
		phase := float64(phase(pos)) / 256

		fmt.Printf("%.1f %.4f", result, phase)
		for _, v := range eval.features {
			fmt.Printf(" %d", v)
		}
		fmt.Println()
	}

	// Panic if we stopped for no reason.
	if err := scanner.Err(); err != nil {
		panic(err)
	}
}
