#!/usr/bin/python
# Copyright 2014-2018 The Zurichess Authors. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

import argparse
import chess
import chess.pgn
import random

FLAGS = None

def sample(game):
    if game.headers.get('Termination') in ('unterminated', 'abandoned'):
        # Skip incomplete games.
        return

    result = game.headers['Result']
    if result == "1/2-1/2":
        result = 0.5
    elif result == "1-0":
        result = 1.0
    elif result == "0-1":
        result = 0.0
    else:
        # Skip incomplete games.
        return

    sample = []
    count = 0

    board = game.board()
    for move in game.main_line():
        score = result
        if board.turn == chess.BLACK:
            score = 1-result

        count += 1
        if count <= FLAGS.positions_per_game:
            sample.append((board.fen(), score))
        else:
            pos = random.randrange(count)
            if pos < FLAGS.positions_per_game:
                sample[pos] = (board.fen(), score)

        board.push(move)


    for fen, score in sample:
        print fen, score



def main():
    pgn = open(FLAGS.pgn)

    while True:
        game = chess.pgn.read_game(pgn)
        if game is None:
            break
        sample(game)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--pgn', type=str, required=True,
                        help='Input pgn file')
    parser.add_argument('--positions_per_game', type=int, required=False,
                        help='number of positions to sample per game',
                        default=3)
    FLAGS = parser.parse_args()
    main()
