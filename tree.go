// Copyright 2014-2019 The Zurichess Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"math"
	"sort"

	. "bitbucket.org/zurichess/board"
)

// Child stores a child node.
type Child struct {
	lock uint32 // a lock to verify the child node.
	move Move   // move to reach the child node.
	node *Node  // pointer to the child node.
}

// tryGetNode returns the child node if found.
// if not found, it returns nil.
func (c *Child) tryGetNode() *Node {
	if c.node == nil || c.lock != c.node.lock {
		c.node = nil
		return nil
	}
	return c.node
}

func (c *Child) getOrCreateNode(pos *Position) *Node {
	if c.node == nil || c.lock != c.node.lock {
		var ok bool
		pos.DoMove(c.move)
		if c.node, ok = hashGetPtr(pos); !ok {
			*c.node = MakeNode(pos)
		}
		c.lock = c.node.lock
		pos.UndoMove()
	}
	return c.node
}

// winChance returns the winning probability.
// if the child is missing, returns 0.5 (i.e. draw).
func (c *Child) winChance() float64 {
	if n := c.tryGetNode(); n != nil {
		return n.result.winChance()
	}
	return 0.5
}

func (c *Child) checkMate() int8 {
	if n := c.tryGetNode(); n != nil {
		return n.checkMate
	}
	return 0
}

type Node struct {
	lock    uint32 // position dependent hash to identify when nodes are overridden
	result  Result // number of games
	exploit int8   // true if now exploring

	checkMate int8    // true if move delivers checkmate
	weight    float64 // weight of this node

	children []Child // children, nil if not explored yet
}

func MakeNode(pos *Position) Node {
	weight := sigmoid(evaluate(pos))
	return Node{
		lock:   hashLock(pos),
		weight: weight,
		result: Result{uint32(weight*24 + 0.5), 24},
	}
}

// pickChildHelper picks a child with highest PUCT score.
//
// PUCT algorithm is used by AlphaZero.
// PUCT paper http://www.ecmlpkdd2013.org/wp-content/uploads/2013/07/456.pdf
func (n *Node) pickChildHelper(pos *Position, depth int, buf []Move) *Child {
	// All children were visited before so we have enough statistics.
	// Get the node with highest confidence interval.
	β := 0.065
	if n.result.numGames() > 100 {
		β = 0.050
	}
	if depth == 0 {
		β += 0.015
	}
	β += 0.75 / float64(len(n.children))

	l := β * math.Sqrt(float64(n.result.numGames()))
	b, bPUCT := -1, float64(0)

	for i := range n.children {
		p := &n.children[i]
		if n.exploit == 2 && p.move.IsQuiet() {
			// First node visit only look at violent moves.
			continue
		}

		c := p.getOrCreateNode(pos)
		if p.move.IsViolent() &&
			uint64(c.result.total-c.result.points)*uint64(n.result.total) < uint64(n.result.points)*uint64(c.result.total) {
			n.weight = math.Max(n.weight, 1-c.weight)
		}

		iPUCT := (1 - c.result.winChance()) + (1-c.weight)*l/float64(1+c.result.numGames())
		if b == -1 || iPUCT > bPUCT {
			b, bPUCT = i, iPUCT
		}
	}

	if n.exploit == 2 {
		n.exploit = 3
		if b == -1 {
			// In case there were no violent moves, return one quiet.
			return n.pickChildHelper(pos, depth, buf)
		}
	}

	if b == -1 {
		panic("no child selected")
	}

	return &n.children[b]
}

// pickChild picks one of the child node using PUCT algorithm.
func (n *Node) pickChild(pos *Position, depth int, buf []Move) (*Node, Move) {
	n.genChildren(pos, buf)
	if len(n.children) == 0 {
		return nil, NullMove
	}

	c := n.pickChildHelper(pos, depth, buf)
	return c.getOrCreateNode(pos), c.move
}

func (n *Node) genChildren(pos *Position, buf []Move) {
	if n.children != nil {
		return
	}

	moves := buf[:0]
	pos.GenerateMoves(Quiet|Violent, &moves)
	n.children = make([]Child, 0, len(moves))
	for _, m := range moves {
		pos.DoMove(m)
		if !pos.IsChecked(pos.Them()) {
			n.children = append(n.children, Child{lock: hashLock(pos), move: m})
		}
		pos.UndoMove()
	}
}

// sortChildren sorts children in decreasing order of weight.
func (n *Node) sortChildren() {
	sort.Slice(n.children, func(i, j int) bool {
		pi := &n.children[i]
		pj := &n.children[j]
		cmi := pi.checkMate()
		cmj := pj.checkMate()

		if cmi == 0 && cmj == 0 {
			// Sort by the winning chance, if mate is not known.
			return pi.winChance() < pj.winChance()
		}

		// One child is lost, but the other is not.
		if cmi < 0 && cmj >= 0 {
			return true
		}
		if cmi >= 0 && cmj < 0 {
			return false
		}
		// One child is won, but the other is not.
		if cmi > 0 && cmj <= 0 {
			return false
		}
		if cmi <= 0 && cmj > 0 {
			return true
		}

		if cmi == 0 || cmj == 0 {
			panic("both must be endgames")
		}

		// Both children are lost or won.
		// If both lost, take the shortest one.
		// If both won, take the longest one.
		return cmi > cmj
	})
}
